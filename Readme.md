This is the API for the listing service. It will be responsible for storing and providing things like name, description, references to images, primary genre of music, type of bar / club, comments, likes, dislikes, etc.

It will not use authentication (for now) - This is called a "squishy centre" which is not advised for production, but at this stage of development it improves developer experience.

Draft doc: https://app.swaggerhub.com/apis/Rock-On.Space/Listing-Service/1.0.0
