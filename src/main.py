import logging

from fastapi import FastAPI, Response

import v1.router
from database.models import ListingDB
from v1.models import Health

app = FastAPI()
app.include_router(v1.router.router)


@app.get("/alive")
async def alive():
    return {"message": "Hello World"}


@app.get("/healthz", response_model=Health, status_code=200)
async def healthz(response: Response):
    health = Health(errors=[])
    try:
        ListingDB.select().page(0, 1)
    except Exception as e:
        response.status_code = 500
        logging.exception("failure to connect to database %s", type(e))
        health.errors.append("Error connecting to DB")
    return health
