import re
from enum import StrEnum, auto
from typing import List, Optional, Iterable, Set
from datetime import datetime
from uuid import UUID

from pydantic import BaseModel, validator, Field, NoneStr
from pony.orm import Set as PonySet
from pony.orm.core import Query

from config import settings
from database.models import (
    open_or_closed,
    Repeating,
    ListingDB,
    PhotoDB,
    OpeningTimeDB,
)


class Health(BaseModel):
    service_name: str = settings.app_name
    version_number: str = settings.version_number
    errors: List[str]


# Todo Publish JSON PATCH as a microlib


class JsonPatchOperations(StrEnum):
    add = auto()  # type: ignore
    remove = auto()  # type: ignore
    replace = auto()  # type: ignore
    move = auto()  # type: ignore
    copy = auto()  # type: ignore
    test = auto()  # type: ignore


class JsonPatch(BaseModel):
    """
    RFC 6902 - JavaScript Object Notation (JSON) Patch

    This represents a single line of the patch, the standard calls for a JSON Array of these.
    """

    value: Optional[object]
    path: str
    from_path: Optional[str] = Field(None, alias="from")
    op: JsonPatchOperations

    @validator("path")
    def check_path(cls, path):
        if not re.match(r"(^/\S*$)", path):
            raise ValueError("invalid path")
        return path

    @validator("from_path")
    def check_from_path(
        cls, from_path, values, **kwargs
    ):  # pylint:disable=unused-argument
        if values is None:
            values = {}
        if not re.match(r"(^/\S*$)", from_path) and from_path != values["path"]:
            raise ValueError("invalid path")
        return from_path

    @validator("op")
    def check_op(cls, op, values, **kwargs):  # pylint:disable=unused-argument
        if values is None:
            values = {}
        if op in ("move", "copy") and "from_path" not in values:
            raise ValueError("missing from")
        return op


class PatchResponse(BaseModel):
    count: int
    successes: int
    errors: List[str]


class OpeningTime(BaseModel):
    uid: Optional[UUID]
    open: datetime
    close: datetime
    repeating: Repeating

    class Config:
        orm_mode = True


class OpeningTimeArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[OpeningTime]

    @classmethod
    def from_query(cls, query: Query, path: str, page: int = 1):
        num_results = query.count()
        opening_times_raw: Iterable[OpeningTimeDB] = query.page(page, 50)
        opening_times: List[OpeningTime] = []
        for opening_time_raw in opening_times_raw:
            opening_time = OpeningTime.from_orm(opening_time_raw)
            opening_times.append(opening_time)
        next_page = (
            f"{path}/?page={page + 1}" if num_results - (page + 1) * 50 > 0 else None
        )
        return cls(_next=next_page, _count=num_results, data=opening_times)


class Geometry(BaseModel):
    type: str
    coordinates: List[float]

    @validator("coordinates")
    def check(cls, coords):
        if len(coords) == 2:
            return coords
        raise ValueError("ensure this value is a list of 2 numbers")


class Photo(BaseModel):
    uid: Optional[UUID]
    photo: str
    alt: str

    class Config:
        orm_mode = True


class PhotoArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[Photo]

    @classmethod
    def from_query(cls, query: Query, path: str, page: int = 1):
        num_results = query.count()
        photos_raw: Iterable[PhotoDB] = query.page(page, 50)
        photos: List[Photo] = []
        for photo_raw in photos_raw:
            photo = Photo.from_orm(photo_raw)
            photos.append(photo)
        next_page = (
            f"{path}/?page={page + 1}" if num_results - (page + 1) * 50 > 0 else None
        )
        return cls(_next=next_page, _count=num_results, data=photos)


class ListingMinimal(BaseModel):
    uid: UUID
    name: str
    icon: str
    genres: Set[str]
    type: str
    tags: Set[str]
    open: Optional[bool]

    class Config:
        orm_mode = True


class Geo(BaseModel):
    type: str
    geometry: Geometry
    properties: Optional[ListingMinimal]


class ListingCondensed(ListingMinimal):
    geo: Geo

    @classmethod
    def from_orm(cls, listing: ListingDB) -> "ListingCondensed":
        temp = {}
        for key in cls.__fields__:
            if (val := getattr(listing, key, None)) is not None:
                temp[key] = val
        temp["open"] = open_or_closed(listing)
        minimal = ListingMinimal.from_orm(listing)
        minimal.open = temp["open"]
        temp["geo"] = Geo(
            type="Feature",
            geometry=Geometry(type="Point", coordinates=listing.coordinates),
            properties=minimal,
        )
        return cls(**temp)


class ListingCondensedArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[ListingCondensed]


class ListingComplete(ListingCondensed):
    description: str
    address: str
    price_category: str
    rating: float
    photos: PhotoArray
    sensors: Optional[List[UUID]] = Field(default_factory=set)
    header_photo: Optional[str]
    opening_times: OpeningTimeArray

    @classmethod
    def from_orm(cls, listing: ListingDB) -> "ListingComplete":
        temp = {}
        for key in cls.__fields__:
            if ((val := getattr(listing, key, None)) is not None) and (
                not isinstance(val, PonySet)
            ):
                temp[key] = val
        temp["photos"] = PhotoArray.from_query(
            listing.photos, f"/listing/{listing.uid}/photo"
        )
        temp["sensors"] = [sensor.uid for sensor in listing.sensors]
        temp["opening_times"] = OpeningTimeArray.from_query(
            listing.opening_times, f"/listing/{listing.uid}/opening_time"
        )
        temp["open"] = open_or_closed(listing)
        minimal = ListingMinimal.from_orm(listing)
        minimal.open = temp["open"]
        temp["geo"] = Geo(
            type="Feature",
            geometry=Geometry(type="Point", coordinates=listing.coordinates),
            properties=minimal,
        )
        return cls(**temp)


class Error(BaseModel):
    code: int
    message: str
