from math import cos
from typing import List, Set, Optional, Annotated
from uuid import UUID

from fastapi import APIRouter, HTTPException, Query
from pony.orm import db_session, select, desc

from database.models import ListingDB, PhotoDB, OpeningTimeDB, SensorDB
from .models import (
    ListingComplete,
    Photo,
    ListingCondensed,
    ListingCondensedArray,
    PhotoArray,
    OpeningTimeArray,
    OpeningTime,
    JsonPatch,
    JsonPatchOperations,
    PatchResponse,
)
from .patch_operations import patch_add, patch_remove, patch_replace

router = APIRouter(
    prefix="/v1",
    tags=["v1"],
)


# noinspection PyChainedComparisons
@router.get(
    "/listing", response_model=ListingCondensedArray, response_model_exclude_none=True
)
async def get_listings(
    tags: Annotated[list[str] | None, Query()] = None,
    exclude_tags: Annotated[list[str] | None, Query()] = None,
    lat: Optional[float] = None,
    lon: Optional[float] = None,
    radius: Optional[float] = None,
    rating: Optional[str] = None,  # pylint: disable=unused-argument
    page: int = 1,
):
    with db_session:
        if lat is not None and lon is not None and radius is not None:
            max_lat = lat + (radius / 110.574)
            min_lat = lat - (radius / 110.574)
            max_lon = lon + (radius / 111.320 * cos(lat))
            min_lon = lon - (radius / 111.320 * cos(lat))
            query = ListingDB.select(
                lambda listing_item: listing_item.coordinates[0] >= min_lon
                and listing_item.coordinates[1] >= min_lat
                and listing_item.coordinates[0] <= max_lon
                and listing_item.coordinates[1] <= max_lat
            )  # Using greate circles is slow, squares are good enough.
        elif (lat, lon, radius) != (None, None, None):
            raise HTTPException(
                status_code=422,
                detail="unprocessable request: if using position then `lat`, `lon`, and `radius` must be present",
            )
        else:
            query = ListingDB.select()
        if tags is not None:
            query = query.filter(lambda listing_item: tags in listing_item.tags)
        if exclude_tags is not None:
            query = query.filter(
                lambda listing_item: exclude_tags not in listing_item.tags
            )
        query.order_by(desc(ListingDB.uid))
        num_results = query.count()
        listings_raw: List[ListingDB] = query.page(page, 50)
        listings_condensed: List[ListingCondensed] = []
        for listing in listings_raw:
            condensed = ListingCondensed.from_orm(listing)
            listings_condensed.append(condensed)
        next_page = f"?page={page+1}" if num_results - (page + 1) * 50 > 0 else None
        return ListingCondensedArray(
            _next=next_page, _count=num_results, data=listings_condensed
        )


@router.post("/listing")
async def add_listing(listing: ListingComplete):
    with db_session():
        listing_db = ListingDB(
            **listing.dict(exclude={"geo", "photos", "opening_times", "open"}),
            coordinates=listing.geo.geometry.coordinates,
        )
        for opening_time in listing.opening_times.data:
            OpeningTimeDB(
                open=opening_time.open,
                close=opening_time.close,
                open_time=opening_time.open.time(),
                close_time=opening_time.close.time(),
                repeating=opening_time.repeating,
                listing=listing_db,
            )
        for photo in listing.photos.data:
            PhotoDB(**photo.dict(exclude={"uid"}), listing=listing_db)
        return


@router.get(
    "/listing/{uid}", response_model=ListingComplete, response_model_exclude_none=True
)
async def get_listing_by_uid(uid: UUID):
    with db_session:
        if (listing := ListingDB.get(uid=uid)) is None:
            raise HTTPException(status_code=404, detail="Listing not found")
        return ListingComplete.from_orm(listing)


@router.patch(
    "/listing/{uid}", response_model_exclude_none=True, response_model=ListingComplete
)
async def patch_listing_by_uid(uid: UUID, body: List[JsonPatch]):
    with db_session:
        if (listing := ListingDB.get(uid=uid)) is None:
            raise HTTPException(status_code=404, detail="Listing not found")
        errors = []
        for patch in body:
            try:
                match patch.op:
                    case JsonPatchOperations.add:
                        await patch_add(listing, patch)
                    case JsonPatchOperations.remove:
                        await patch_remove(listing, patch)
                    case JsonPatchOperations.replace:
                        await patch_replace(listing, patch)
                    case _:
                        raise ValueError(f"unsupported operation: '{patch.op}' ")
            except ValueError as e:
                errors.append(str(e.args))
        return PatchResponse(
            count=len(body), successes=len(body) - len(errors), errors=errors
        )


@router.get(
    "/listing/{listing_uid}/sensor",
    response_model=Set[UUID],
)
async def get_sensors(listing_uid: UUID):
    with db_session:
        if (listing := ListingDB.get(uid=listing_uid)) is None:
            raise HTTPException(status_code=404, detail="Listing not found")
        return {sensor.uid for sensor in listing.sensors}


@router.post("/listing/{listing_uid}/sensor", status_code=201)
async def add_sensor(listing_uid: UUID, body: UUID):
    with db_session:
        if (listing := ListingDB.get(uid=listing_uid)) is None:
            raise HTTPException(status_code=404, detail="Listing not found")
        if ((sensor := SensorDB.get(uid=body)) is not None) and (
            sensor.listing != listing
        ):
            raise HTTPException(
                status_code=409, detail="Sensor already registered to another Listing"
            )
        SensorDB(listing=listing, uid=sensor)
        return


@router.delete("/listing/{listing_uid}/sensor/{sensor_uid}", status_code=201)
async def delete_sensor(listing_uid: UUID, sensor_uid: UUID):
    with db_session:
        if (listing := ListingDB.get(uid=listing_uid)) is None:
            raise HTTPException(status_code=404, detail="Listing not found")
        if (sensor := SensorDB.get(listing=listing, uid=sensor_uid)) is not None:
            sensor.delete()
        return


@router.get(
    "/listing/{listing_uid}/photo",
    response_model_exclude_none=True,
    response_model=PhotoArray,
)
async def get_photos(listing_uid: UUID, page: int = 1):
    with db_session:
        if (
            query := select(
                p
                for p in PhotoDB  # type: ignore
                for listing in ListingDB  # type: ignore
                if p.listing == listing and listing.uid == listing_uid
            )
        ).count() == 0:
            raise HTTPException(status_code=404, detail="Listing not found")
        return PhotoArray.from_query(query, f"/listing/{listing_uid}/photo", page)


@router.get("/listing/{listing_uid}/photo/{photo_uid}", response_model=Photo)
def get_photo(listing_uid: UUID, photo_uid: UUID):
    with db_session:
        if (
            photo := select(
                p
                for p in PhotoDB  # type: ignore
                for listing in ListingDB  # type: ignore
                if p.listing == listing
                and p.uid == photo_uid
                and listing.uid == listing_uid
            ).first()
        ) is None:
            raise HTTPException(status_code=404, detail="Photo not found")
        return Photo.from_orm(photo)


@router.delete("/listing/{listing_uid}/photo/{photo_uid}", status_code=201)
def delete_photo(listing_uid: UUID, photo_uid: UUID):
    with db_session:
        if (
            photo := select(
                p
                for p in PhotoDB  # type: ignore
                for listing in ListingDB  # type: ignore
                if p.listing == listing
                and p.uid == photo_uid
                and listing.uid == listing_uid
            ).first()
        ) is None:
            return
        photo.delete()


@router.post(
    "/listing/{uid}/photo",
    status_code=201,
)
def add_photo(uid: str, body: Photo):
    with db_session:
        if (listing := ListingDB.get(uid=uid)) is None:
            raise HTTPException(status_code=404, detail="Listing not found")
        PhotoDB(**body.dict(exclude={"uid"}), listing=listing)
        return


@router.get(
    "/listing/{listing_uid}/opening_time",
    response_model_exclude_none=True,
    response_model=OpeningTimeArray,
)
async def get_opening_times(listing_uid: UUID, page: int = 1):
    with db_session:
        if (
            query := select(
                o
                for o in OpeningTimeDB  # type: ignore
                for listing in ListingDB  # type: ignore
                if o.listing == listing and listing.uid == listing_uid
            )
        ).count() == 0:
            raise HTTPException(status_code=404, detail="Listing not found")
        return OpeningTimeArray.from_query(
            query, f"/listing/{listing_uid}/opening_time", page
        )


@router.get(
    "/listing/{listing_uid}/opening_time/{opening_time_uid}", response_model=OpeningTime
)
def get_opening_time(listing_uid: UUID, opening_time_uid: UUID):
    with db_session:
        if (
            opening_time := select(
                o
                for o in OpeningTimeDB  # type: ignore
                for listing in ListingDB  # type: ignore
                if o.listing == listing
                and o.uid == opening_time_uid
                and listing.uid == listing_uid
            ).first()
        ) is None:
            raise HTTPException(status_code=404, detail="OpeningTime not found")
        return OpeningTime.from_orm(opening_time)


@router.delete(
    "/listing/{listing_uid}/opening_time/{opening_time_uid}",
    status_code=201,
)
def delete_opening_time(listing_uid: UUID, opening_time_uid: UUID):
    with db_session:
        if (
            opening_time := select(
                o
                for o in OpeningTimeDB  # type: ignore
                for listing in ListingDB  # type: ignore
                if o.listing == listing
                and o.uid == opening_time_uid
                and listing.uid == listing_uid
            ).first()
        ) is None:
            return
        opening_time.delete()


@router.post(
    "/listing/{uid}/opening_time",
    response_model_exclude_none=True,
    status_code=201,
)
def add_opening_time(uid: str, body: OpeningTime):
    with db_session:
        if (listing := ListingDB.get(uid=uid)) is None:
            raise HTTPException(status_code=404, detail="Listing not found")
        OpeningTimeDB(
            open=body.open,
            close=body.close,
            open_time=body.open.time(),
            close_time=body.close.time(),
            repeating=body.repeating,
            listing=listing,
        )
        return
