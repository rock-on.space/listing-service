from typing import cast, List
from uuid import UUID

from database.models import OpeningTimeDB, PhotoDB, ListingDB, PriceCategory, SensorDB
from .models import JsonPatch, Photo, OpeningTime


async def patch_add(listing: ListingDB, patch: JsonPatch):
    match patch.path:
        case "/opening_times/data":
            value = cast(dict, patch.value)
            opening_time = OpeningTime.parse_obj(value)
            OpeningTimeDB(
                open=opening_time.open,
                close=opening_time.close,
                open_time=opening_time.open.time(),
                close_time=opening_time.close.time(),
                repeating=opening_time.repeating,
                listing=listing,
            )
        case "/photos/data":
            photo = Photo.parse_obj(patch.value)
            PhotoDB(photo=photo.photo, alt=photo.alt, listing=listing)
        case "/sensors":
            if not isinstance(patch.value, UUID):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            SensorDB(uid=patch.value, listing=listing)
        case "/genre":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            if patch.value not in listing.genre:
                listing.genre.append(patch.value)  # ToDo: add genre input validation
        case "/tags":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            if patch.value not in listing.tags:
                listing.tags.add(patch.value)  # ToDo: add tags input validation
        case "/header_photo":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            listing.header_photo = patch.value  # ToDo: add path input validation
        case _:
            raise ValueError(
                f"unsupported operation '{patch.op}' for path '{patch.path}'"
            )


async def patch_remove(listing: ListingDB, patch: JsonPatch):
    match patch.path:
        case "/opening_times/data":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"unsupported datatype for delete operation at path '{patch.path}', must be object's uid"
                )
            opening_time = OpeningTimeDB.get(listing=listing, uid=patch.value)
            if opening_time is not None:
                opening_time.delete()
        case "/photos/data":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"unsupported datatype for delete operation at path '{patch.path}', must be object's uid"
                )
            photo = PhotoDB.get(listing=listing, uid=cast(str, patch.value).lower())
            if photo is not None:
                photo.delete()
        case "/sensors":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"unsupported datatype for delete operation at path '{patch.path}', must be object's uid"
                )
            sensor = SensorDB.get(listing=listing, uid=cast(str, patch.value).lower())
            if sensor is not None:
                sensor.delete()
        case "/genre":
            value = cast(str, patch.value).lower()
            listing.genre.remove(value)
        case "/tags":
            value = cast(str, patch.value).lower()
            listing.tags.remove(value)
        case "/header_photo":
            listing.header_photo = ""
        case _:
            raise ValueError(
                f"unsupported operation '{patch.op}' for path '{patch.path}'"
            )


async def patch_replace(listing: ListingDB, patch: JsonPatch):
    match patch.path:
        case "/name":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            listing.name = patch.value
        case "/icon":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            listing.icon = patch.value
        case "/type":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            listing.type = patch.value
        case "/description":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            listing.description = patch.value
        case "/address":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            listing.address = patch.value
        case "/price_category":
            listing.price_category = PriceCategory(patch.value)
        case "/rating":
            if not isinstance(patch.value, float):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            listing.rating = patch.value
        case "/header_photo":
            listing.header_photo = patch.value  # ToDo: add path input validation
        case "/geo/geometry/coordinates":
            value_float_list = cast(List[float], patch.value)
            if len(value_float_list) != 2:
                raise ValueError(f"invalid length for path '{patch.path}'")
            listing.coordinates = value_float_list
        case _:
            raise ValueError(
                f"unsupported operation '{patch.op}' for path '{patch.path}'"
            )
