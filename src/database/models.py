from datetime import datetime, time
from enum import StrEnum, auto
from uuid import UUID, uuid4

from pony.orm import Database, StrArray, FloatArray
from pony.orm import PrimaryKey as PonyPrimaryKey
from pony.orm import Required as PonyRequired
from pony.orm import Set as PonySet

from config import settings

db = Database()


class Repeating(StrEnum):
    Daily = auto()
    Weekly = auto()
    No = auto()


class PriceCategory(StrEnum):
    Cheap = auto()
    Average = auto()
    Expensive = auto()


class ListingDB(db.Entity):  # type: ignore
    uid = PonyPrimaryKey(UUID)
    name = PonyRequired(str)
    icon = PonyRequired(str)
    genres = PonyRequired(StrArray)
    type = PonyRequired(str)
    tags = PonyRequired(StrArray)
    description = PonyRequired(str)
    address = PonyRequired(str)
    price_category = PonyRequired(PriceCategory)
    rating = PonyRequired(float)
    opening_times = PonySet("OpeningTimeDB")  # type: ignore
    header_photo = PonyRequired(str)
    coordinates = PonyRequired(FloatArray)
    photos = PonySet("PhotoDB")  # type: ignore
    sensors = PonySet("SensorDB")  # type: ignore


class PhotoDB(db.Entity):  # type: ignore
    uid = PonyPrimaryKey(UUID, default=uuid4)
    listing = PonyRequired(ListingDB)
    photo = PonyRequired(str)
    alt = PonyRequired(str)


class SensorDB(db.Entity):  # type: ignore
    uid = PonyPrimaryKey(UUID, default=uuid4)
    listing = PonyRequired(ListingDB)


class OpeningTimeDB(db.Entity):  # type: ignore
    uid = PonyPrimaryKey(UUID, default=uuid4)
    listing = PonyRequired(ListingDB)
    open = PonyRequired(datetime, volatile=True)
    close = PonyRequired(datetime, volatile=True)
    open_time = PonyRequired(time, volatile=True)
    close_time = PonyRequired(time, volatile=True)
    repeating = PonyRequired(Repeating)


match settings.db_provider:
    case "sqlite":
        db.bind(
            provider=settings.db_provider, filename=settings.db_name, create_db=True
        )
    case "postgres":
        db.bind(
            provider=settings.db_provider,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_username,
            password=settings.db_password,
            database=settings.db_name,
            sslmode=settings.db_sslmode,
        )
    case "cockroach":
        db.bind(
            provider=settings.db_provider,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_username,
            password=settings.db_password,
            database=settings.db_name,
            sslmode=settings.db_sslmode,
        )
    case _:
        raise ValueError("Unknown database provider")


db.generate_mapping(create_tables=settings.db_create_tables)


def open_or_closed(listing: ListingDB):
    now = datetime.utcnow()
    for opening_time in listing.opening_times:
        open_iso = opening_time.open
        close_iso = opening_time.close
        match opening_time.repeating:
            case Repeating.Daily:
                today_date = now.date()
                offset = close_iso.date() - open_iso.date()
                open_datetime = datetime.combine(today_date, open_iso.time())
                close_datetime = datetime.combine(today_date + offset, close_iso.time())
                if open_datetime <= now < close_datetime:
                    return True  # Not directly returning result of comparison because we're in a loop
            case Repeating.Weekly:
                today_weekday = now.isoweekday()
                day_offset = (close_iso.date() - open_iso.date()).days
                close_utc = close_iso.utctimetuple()
                close_total_seconds = (
                    close_utc.tm_hour * 60 * 60
                    + close_utc.tm_min * 60
                    + close_utc.tm_sec
                )
                open_utc = open_iso.utctimetuple()
                open_total_seconds = (
                    open_utc.tm_hour * 60 * 60 + open_utc.tm_min * 60 + open_utc.tm_sec
                )
                now_utc = now.utctimetuple()
                now_total_seconds = (
                    now_utc.tm_hour * 60 * 60 + now_utc.tm_min * 60 + now_utc.tm_sec
                )
                time_offset = (close_total_seconds - open_total_seconds) % (
                    24 * 60 * 60
                )
                if (
                    open_iso.isoweekday()
                    <= today_weekday
                    <= open_iso.isoweekday() + day_offset
                ) and (
                    open_total_seconds
                    <= now_total_seconds
                    < open_total_seconds + time_offset
                ):
                    return True  # Not directly returning result of comparison because we're in a loop
            case Repeating.No:
                if open_iso <= now < close_iso:
                    return True  # Not directly returning result of comparison because we're in a loop
    return False
