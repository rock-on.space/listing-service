from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = "ListingService"
    version_number: str = "local"
    db_provider: str = "sqlite"
    db_name: str = "database.sqlite"
    db_username: str = "postgres"
    db_password: str = ""
    db_host: str = "localhost"
    db_port: int = 5432
    db_sslmode: str = "require"
    db_create_tables: bool = True


settings = Settings()
